//Marks completed task
$("ul").on("click","li",function () {
    $(this).toggleClass('completed');
});
//Deletes element after fading out
$("ul").on("click","span",function (event) {
    $(this).parent().fadeOut(500, function () {
        $(this).remove();
    });
    event.stopPropagation();
});

$('input[type="text"').keypress(function (e) {
    if (e.which === 13) {
        //Grabbing new element text
       let todoText= $(this).val();
        $(this).val("");
       //create a new li and add to ul
        $('ul').append(`<li><span><i class="fa fa-trash"></i></span> ${todoText}</li>`)

    }
});
//Hides and show added input
$("#toggle-form").click(function(){
    $("input[type='text']").fadeToggle();
});