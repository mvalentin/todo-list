 # Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


##[v1.0.0] -2020-04-20
## Added:
-Ability to crossout done tasks.
-Ability to delete tasks.
-Ability to add tasks.
-Animations.
-Font Awesome Support.

## Changed:
- Change to jQuery 3.5.0.slim instead of .slim.min to fix fadeOut() animation.

## [v0.1.0] - 2020-04-11
 ### Added:
 - Redone barebone structure
 - Adds jquery-3.5.0.slim.min under assets/js/lib.
 - Adds todos.js for main app program.
 - Adds main.css under assets/css.

## [v0.0.1] - 2020-04-11
 ### Added:
- Created Readme, Changelog and .gitignore file.
- Adds main app template.
- Adds console barebone funtional app.

[v1.0.0]:https://gitlab.com/mvalentin/todo-list/-/tags/v1.0.0
[v0.1.0]:https://gitlab.com/mvalentin/todo-list/-/tags/v0.1.0
[v0.0.1]:https://gitlab.com/mvalentin/todo-list/-/tags/v0.0.1